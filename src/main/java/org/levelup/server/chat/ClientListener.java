package org.levelup.server.chat;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.net.ServerSocket;

@RequiredArgsConstructor
public class ClientListener implements Runnable {

    private final ServerSocket server;

    private volatile boolean running = true;

    @Override
    public void run() {
        while (running) {
            try {
                server.accept();
            } catch (IOException ignored) {}
        }
    }

    public void shutdown() {
        try {
            running = false;
            server.close();
        } catch (IOException exc) {
            System.out.println("Server stopped...");
        }
    }

}
