package org.levelup.server.chat.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "rooms")
@NoArgsConstructor
public class Room {

    @Id
    @SequenceGenerator(
            name = "room_id_generator",     // Название генератора, который используется в аннотации @GeneratedValue
            sequenceName = "room_id_seq",   // Название таблицы/последовательности, которая создастся в БД
            initialValue = 10000
    )
    @GeneratedValue(generator = "room_id_generator", strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(unique = true, nullable = false)
    private String name;

    public Room(String name) {
        this.name = name;
    }
}
