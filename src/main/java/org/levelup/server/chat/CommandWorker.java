package org.levelup.server.chat;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.levelup.server.chat.command.CommandParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@RequiredArgsConstructor
public class CommandWorker implements Runnable {

    private final BufferedReader console =
            new BufferedReader(new InputStreamReader(System.in));
    private final ChatServer chatServer;

    @Override
    public void run() {
        try {
            String line;
            CommandParser commandParser = new CommandParser();
            while (!"stop".equalsIgnoreCase((line = console.readLine()))) {
                commandParser.executeCommand(line);
            }
            chatServer.stopServer();
        } catch (IOException  exc) {
            exc.printStackTrace();
        }
    }

}
