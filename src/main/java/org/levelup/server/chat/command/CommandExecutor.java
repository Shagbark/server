package org.levelup.server.chat.command;

public interface CommandExecutor {

    void execute();

}
