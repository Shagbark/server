package org.levelup.server.chat.command;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CommandParser {

    private Map<String, CommandExecutor> executors;

    {
        executors = new HashMap<>();
        executors.put("rooms", new GetRoomsCommandExecutor());
    }

    public void executeCommand(String line) {
        // switch-case, if...else
        CommandExecutor executor = executors.get(line);
        if (executor != null) {
            executor.execute();
        } else {
            System.out.println("Вы ввели неверную команду.");
        }

//        Optional.of(executors.get(line)).orElseThrow(RuntimeException::new).execute();
    }

}
